package de.web.basman93;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.plugin.java.JavaPlugin;

public class KickPlayerOnJoin extends JavaPlugin implements Listener {

	private boolean active = true;
	private boolean blacklist = false;
	private int config_version = 0;
	private FileConfiguration config = getConfig();
	private List<String> playerlist = new ArrayList<String>();
	private final String prefix = ChatColor.GRAY + "[K" + ChatColor.DARK_GRAY + "P" + ChatColor.GRAY + "OJ] ";

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("kickplayeronjoin")) {

			if(args.length > 0 && args[0].equalsIgnoreCase("on") && cop(sender, "kpoj.mode")) {
				sender.sendMessage(prefix + "activated");
				active = true;
				save();

			} else if(args.length > 0 && args[0].equalsIgnoreCase("off") && cop(sender, "kpoj.mode")) {
				sender.sendMessage(prefix + "deactivated");
				active = false;
				save();

			} else if(args.length > 0 && args[0].equalsIgnoreCase("help") && cop(sender, "kpoj.help")) {
				sender.sendMessage(prefix + "---------------------- HELP ----------------------");
				sender.sendMessage(prefix + "/kpoj              - shows version & status");
				sender.sendMessage(prefix + "/kpoj version      - shows version");
				sender.sendMessage(prefix + "/kpoj help         - shows this help info");
				sender.sendMessage(prefix + "/kpoj off          - deactivate this plugin");
				sender.sendMessage(prefix + "/kpoj on           - activate this plugin");
				sender.sendMessage(prefix + "/kpoj add <player> - add player to whitelist");
				sender.sendMessage(prefix + "/kpoj del <player> - removes player from whitelist");
				sender.sendMessage(prefix + "/kpoj reload       - reload plugin");
				sender.sendMessage(prefix + "/kpoj list         - list players on whitelist");
				sender.sendMessage(prefix + "/kpoj mode <mode>  - Change the mode of the plugin (whitelist|blacklist)");
				sender.sendMessage(prefix + "--------------------------------------------------");

			} else if(args.length > 0 && args[0].equalsIgnoreCase("version")) {
				sender.sendMessage(prefix + "Version: " + getDescription().getVersion());

			} else if(args.length > 1 && args[0].equalsIgnoreCase("add") && cop(sender, "kpoj.add")) {
				for(int i = 1; i < args.length; i++) {
					if(playerlist.contains(args[i])) {
						sender.sendMessage(prefix + args[i] + " is already in the " + (blacklist ? "blacklist" : "whitelist") + "!");
					} else {
						playerlist.add(args[i]);
						sender.sendMessage(prefix + args[i] + " add to " + (blacklist ? "blacklist" : "whitelist") + "!");
						save();
					}
				}

			} else if(args.length > 1 && args[0].equalsIgnoreCase("del") && cop(sender, "kpoj.del")) {
				for(int i = 1; i < args.length; i++) {
					if(playerlist.contains(args[i])) {
						playerlist.remove(args[i]);
						sender.sendMessage(prefix + args[i] + " removed from the " + (blacklist ? "blacklist" : "whitelist") + "!");
						save();
					} else {
						sender.sendMessage(prefix + args[i] + " is not in the " + (blacklist ? "blacklist" : "whitelist") + "!");
					}
				}

			} else if(args.length > 0 && args[0].equalsIgnoreCase("reload") && cop(sender, "kpoj.reload")) {
				reloadConfig();
				config = getConfig();
				playerlist = config.getStringList("Player");
				active = config.getBoolean("Check");
				blacklist = config.getBoolean("Blacklist");
				config_version = config.getInt("Version");
				sender.sendMessage(prefix + "Config reloaded!");

			} else if(args.length > 0 && args[0].equalsIgnoreCase("list") && cop(sender, "kpoj.list")) {
				sender.sendMessage(prefix + "Players: " + String.join(", ", playerlist));

			} else if(args.length > 1 && args[0].equalsIgnoreCase("mode") && cop(sender, "kpoj.listmode")) {
				if(args[1].matches("w|white|whitelist")) {
					if(blacklist) {
						blacklist = false;
						sender.sendMessage(prefix + "Changed to mode: whitelist");
						save();
					}
					else
						sender.sendMessage(prefix + "Mode already whitelist");

				} else if(args[1].matches("b|black|blacklist")) {
					if(!blacklist) {
						blacklist = true;
						sender.sendMessage(prefix + "Changed to mode: blacklist");
						save();
					}
					else
						sender.sendMessage(prefix + "Mode already blacklist");

				}

			} else {
				sender.sendMessage(prefix + "Version: " + getDescription().getVersion());
				if(cop(sender, "kpoj.info")) {
					sender.sendMessage(prefix + "Active:  " + (active ? ChatColor.GREEN + "yes" : ChatColor.RED + "no"));
					sender.sendMessage(prefix + "Mode:    " + (blacklist ? ChatColor.GRAY + "blacklist" : ChatColor.WHITE + "whitelist"));
				}

				if(sender.hasPermission("kpoj.info") && sender instanceof Player) {
					if(blacklist)
						sender.sendMessage(prefix + "Status:  " + (playerlist.contains(sender.getName()) ? ChatColor.RED + "Disallowed" : ChatColor.GREEN + "Allowed"));
					else
						sender.sendMessage(prefix + "Status:  " + (playerlist.contains(sender.getName()) ? ChatColor.GREEN + "Allowed" : ChatColor.RED + "Disallowed"));
				}

			}
			return true;
		}
		return false;
	}

	@Override
    public void onEnable() {
		saveDefaultConfig();

		playerlist = config.getStringList("Player");
		active = config.getBoolean("Check");
		blacklist = config.getBoolean("Blacklist", false);
		config_version = config.getInt("Version", 1);

		if(config_version == 1) {
			getLogger().log(Level.INFO, "Update config from version " + config_version + " to version 2");
			config_version = 2;
			save();
		}

		getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {
		if(active && !event.getPlayer().isOp()) {
			if(blacklist && playerlist.contains(event.getPlayer().getName())) {
				event.disallow(Result.KICK_WHITELIST, "You are black-listed on this server!");
				getServer().broadcastMessage(event.getPlayer().getName() + " tried to join.");
				getLogger().log(Level.INFO, event.getPlayer().getName() + " tried to join but is on the blacklist");
				getLogger().log(Level.INFO, "Stopped player " + event.getPlayer().getName() + " from joining");

			} else if(!blacklist && !playerlist.contains(event.getPlayer().getName())) {
				event.disallow(Result.KICK_WHITELIST, "You are not white-listed on this server!");
				getServer().broadcastMessage(event.getPlayer().getName() + " tried to join.");
				getLogger().log(Level.INFO, event.getPlayer().getName() + " tried to join but is not on the whitelist");
				getLogger().log(Level.INFO, "Stopped player " + event.getPlayer().getName() + " from joining");

			}
		}
	}

	private boolean cop(CommandSender sender, String perm) {
		return sender.hasPermission(perm) || !(sender instanceof Player);
	}

	private void save() {
		config.set("Check", active);
		config.set("Player", playerlist);
		config.set("Blacklist", blacklist);
		config.set("Version", config_version);
		saveConfig();
	}
}
